angular.module('rockola_project', [])
        .controller('filasCanciones', function ($scope, $http) {

            $scope.id = "";
            $scope.nombre = "";
            $scope.anio = "";
            $scope.ruta = "";
            $scope.formato = "";
            $scope.autor = "";
            $scope.genero = "";

            // accion del boton consultar
            $scope.consultare = function () {

                $http.get("/canciones?genero=" + $scope.gender).then(function (data) {
                    console.log(data);
                    $scope.filas = data.data;
                }, function () {
                    $scope.filas = [];
                });
            };

            $scope.consultarTodo = function () {

                $http.get("/all").then(function (data) {
                    $scope.filas = data.data;
                }, function () {
                    $scope.filas = [];
                });
            };


            //acción del botón actualizar
            $scope.actualizar = function () {
                if ($scope.cedula == undefined || $scope.cedula == null) {
                    $scope.cedula = 0;
                }
                data = {
                    "id_cancion": $scope.id,
                    "nombre_cancion": $scope.nombre,
                    "anio": $scope.anio,
                    "formato": $scope.formato,
                    "ruta": $scope.ruta,
                    "autor": $scope.autor,
                    "genero": $scope.genero
                }
                $http.post('/generos', data).then(function (data) {
                    //success
                    $scope.id = data.data.id_cancion;
                    $scope.nombre = data.data.nombre_cancion;
                    $scope.anio = data.data.anio;
                    $scope.formato = data.data.formato;
                    $scope.ruta = data.data.ruta;
                    $scope.autor = data.data.autor;
                    $scope.genero = data.data.genero;
                }, function () {
                    $scope.id = "";
                    $scope.nombre = "";
                    $scope.anio = "";
                    $scope.formato = "";
                    $scope.ruta = "";
                    $scope.autor = "";
                    $scope.genero = "";
                    $scope.filas = [];
                    window.alert("Canción actualizada satisfactoriamente");
                    //consultarTodo()
                    $http.get("/all").then(function (data) {
                        $scope.filas = data.data;
                    }, function () {
                        $scope.filas = [];
                    });
                });
            };

            $scope.prepararActualizacion = function () {
                if ($scope.id === undefined || $scope.id === null) {
                    $scope.id = 1;
                }

                $http.get("/cancion?id_cancion=" + $scope.id).then(function (data) {
                    console.log(data);
                    $scope.id = data.data.id;
                    $scope.nombre = data.data.nombre;
                    $scope.anio = data.data.anio;
                    $scope.formato = data.data.formato;
                    $scope.ruta = data.data.ruta;
                    $scope.autor = data.data.autor;
                    $scope.genero = data.data.genero;
                    console.log(data.data.id_cancion);
                }, function () {
                    $scope.result = [];
                });
            };

            $scope.agregarCancion = function () {

                data = {
                    "nombre_cancion": $scope.nombre,
                    "anio": $scope.anio,
                    "formato": $scope.formato,
                    "ruta": $scope.ruta,
                    "autor": $scope.autor,
                    "genero": $scope.genero
                };
                $http.put('/nueva_cancion', data).then(function (data) {
                    //success
                    $scope.nombre = data.data.nombre_cancion;
                    $scope.anio = data.data.anio;
                    $scope.formato = data.data.formato;
                    $scope.ruta = data.data.ruta;
                    $scope.autor = data.data.autor;
                    $scope.genero = data.data.genero;
                }, function () {
                    $scope.id = "";
                    $scope.nombre = "";
                    $scope.anio = "";
                    $scope.formato = "";
                    $scope.ruta = "";
                    $scope.autor = "";
                    $scope.genero = "";
                    $scope.filas = [];
                    window.alert("Canción ingresada satisfactoriamente");

                    $http.get("/all").then(function (data) {
                        $scope.filas = data.data;
                    }, function () {
                        $scope.filas = [];
                    });
                });
            };

            $scope.borrar = function () {
                if ($scope.id === undefined || $scope.id === null) {
                    $scope.id = 0;
                }
                data = {
                    "id_cancion": $scope.id
                };
                $http.delete("/eliminar_cancion/" + $scope.id, data).then(function () {
                }, function () {
                    alert("Se ha eliminado la canción");
                    $http.get("/all").then(function (data) {
                        $scope.filas = data.data;
                    }, function () {
                        $scope.filas = [];
                    });
                });
            };
            
        });
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.rockola_project.logic;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author isfel
 */
public class usuario {
    @Autowired // Permite la conexión del JdbcTemplate a la base de datos
    JdbcTemplate jdbcTemplate; // Instancia del JdbcTemplate el cual es el equivalente al statement de la clase conexi[on
    // Ventajas: Manejo de excepciones controlado., Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los par[ametros.
    //Atributos

    private int id_usuario;
    private int tipo;
    private String correo;
    private String pass;

    public usuario() {
    }

    public usuario(int id_usuario, int tipo, String correo, String pass) {
        this.id_usuario = id_usuario;
        this.tipo = tipo;
        this.correo = correo;
        this.pass = pass;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    //Guardar
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO `rockola_3.0`.`canciones` (id_usuario,tipo,correo,pass)VALUES(?,?,?,?)";
        return sql;
    }
}

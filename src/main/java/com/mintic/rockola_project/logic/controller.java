/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.rockola_project.logic;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isfel
 */
@Controller
public class controller {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String page1() {
        return "userView.html";    
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/admView")
    public String index() {
        return "admView.html";
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/nuevaCancion")
    public String editar() {
    return "nuevaCancion.html";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mintic.rockola_project.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author isfel
 */
@Component("canciones")
public class cancion {

    @Autowired // Permite la conexión del JdbcTemplate a la base de datos
    JdbcTemplate jdbcTemplate; // Instancia del JdbcTemplate el cual es el equivalente al statement de la clase conexi[on
    // Ventajas: Manejo de excepciones controlado., Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los par[ametros.
    //Atributos    
    private int id_cancion;
    private String nombre_cancion;
    private int anio;
    private String formato;
    private String ruta;
    private String autor;
    private String genero;

    //Constructor
    public cancion(int id_cancion, String nombre_cancion, int anio, String formato, String ruta, String autor, String genero) {
        this.id_cancion = id_cancion;
        this.nombre_cancion = nombre_cancion;
        this.anio = anio;
        this.formato = formato;
        this.ruta = ruta;
        this.autor = autor;
        this.genero = genero;
    }
    //Constructor

    public cancion() {
    }

    //getters y setters
    public int getId_cancion() {
        return id_cancion;
    }

    public void setId_cancion(int id_cancion) {
        this.id_cancion = id_cancion;
    }

    public String getNombre_cancion() {
        return nombre_cancion;
    }

    public void setNombre_cancion(String nombre_cancion) {
        this.nombre_cancion = nombre_cancion;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    //CRUD
    //Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_cancion, nombre_cancion, anio, formato, ruta, autor, genero FROM canciones WHERE id_cancion = ?";
        List<cancion> canciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new cancion(
                        rs.getInt("id_cancion"),
                        rs.getString("nombre_cancion"),
                        rs.getInt("anio"),
                        rs.getString("formato"),
                        rs.getString("ruta"),
                        rs.getString("autor"),
                        rs.getString("genero")
                ), new Object[]{this.getId_cancion()});

        if (canciones != null && canciones.size() > 0) {
            this.setId_cancion(canciones.get(0).getId_cancion());
            this.setNombre_cancion(canciones.get(0).getNombre_cancion());
            this.setAnio(canciones.get(0).getAnio());
            this.setFormato(canciones.get(0).getFormato());
            this.setRuta(canciones.get(0).getRuta());
            this.setAutor(canciones.get(0).getAutor());
            this.setGenero(canciones.get(0).getGenero());
            return true;
        } else {
            return false;
        }
    }

    public List<cancion> consultarTodo(String genero_cancion) throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT id_cancion, nombre_cancion, anio, formato, ruta, autor, genero FROM canciones WHERE genero = ?";
        List<cancion> canciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new cancion(//devuelve una instancia de la cuenta
                        rs.getInt("id_cancion"), // => 123
                        rs.getString("nombre_cancion"), // => 
                        rs.getInt("anio"),
                        rs.getString("formato"),
                        rs.getString("ruta"),
                        rs.getString("autor"),
                        rs.getString("genero")
                ), new Object[]{genero_cancion});
        return canciones;
    }

    public List<cancion> all() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT * FROM canciones";
        List<cancion> canciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new cancion(//devuelve una instancia de la cuenta
                        rs.getInt("id_cancion"),
                        rs.getString("nombre_cancion"),
                        rs.getInt("anio"),
                        rs.getString("formato"),
                        rs.getString("ruta"),
                        rs.getString("autor"),
                        rs.getString("genero")
                ), new Object[]{});
        return canciones;
    }

    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        String sql = "UPDATE canciones SET nombre_cancion = ?, anio = ?, formato = ?, ruta = ?, autor = ?, genero = ? WHERE id_cancion = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre_cancion());
        ps.setInt(2, this.getAnio());
        ps.setString(3, this.getFormato());
        ps.setString(4, this.getRuta());
        ps.setString(5, this.getAutor());
        ps.setString(6, this.getGenero());
        ps.setInt(7, this.getId_cancion());
        ps.executeUpdate();
        ps.close();
    }

    //Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO `rockola_3.0`.`canciones` (nombre_cancion,anio,formato,ruta, autor, genero) VALUES (?,?,?,?,?,?);";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre_cancion());
        ps.setInt(2, this.getAnio());
        ps.setString(3, this.getFormato());
        ps.setString(4, this.getRuta());
        ps.setString(5, this.getAutor());
        ps.setString(6, this.getGenero());
        ps.execute();
        ps.close();
    }

    public void borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM `rockola_3.0`.`canciones` WHERE id_cancion = ?;";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_cancion());
        ps.execute();
        ps.close();
    }

    @Override
    public String toString() {
        return "cancion{ id_cancion=" + id_cancion + ", nombre_cancion=" + nombre_cancion + ", anio=" + anio + ", formato=" + formato + ", ruta=" + ruta + ", autor=" + autor + ", genero=" + genero + '}';
    }
}

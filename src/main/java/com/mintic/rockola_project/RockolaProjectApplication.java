package com.mintic.rockola_project;

import com.mintic.rockola_project.logic.cancion;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@SpringBootApplication
@RestController // la clase va a ser auna API REST
//REST Se basa en una transferencia de recursos
public class RockolaProjectApplication {

    @Autowired
    cancion c;

    public static void main(String[] args) {
        SpringApplication.run(RockolaProjectApplication.class, args);
    }

    @GetMapping("/hola")
    public String hola(@RequestParam(value = "name", defaultValue = "world") String name,
            @RequestParam(value = "age", defaultValue = "29") String age) {
        return String.format("Hello %s, you are %s years old", name, age);
    }

    @GetMapping("/cancion")
    public String consultarCancionPorId(@RequestParam(value = "id_cancion", defaultValue = "0") int id_cancion) throws ClassNotFoundException, SQLException {

        c.setId_cancion(id_cancion);

        if (c.consultar()) {
            return "{\"id\":" + c.getId_cancion() + ",\"nombre\":\"" + c.getNombre_cancion() + "\",\"anio\":" + c.getAnio() + ","
                    + "\"formato\":\"" + c.getFormato() + "\",\"ruta\":\"" + c.getRuta() + "\",\"autor\":\"" + c.getAutor() + "\",\"genero\":\"" + c.getGenero() + "\"}";
        } else {
            return "ID de canción no existe en la base de datos.";
        }
    }

    @GetMapping("/canciones")
    public String consultarCancionesPorGenero(@RequestParam(value = "genero", defaultValue = "1") String genero) throws ClassNotFoundException, SQLException {

        List<cancion> canciones = c.consultarTodo(genero);

        if (canciones.size() > 0) {
            return new Gson().toJson(canciones);
        } else {
            return new Gson().toJson(canciones);
            //return "{\"id\":\""+cedula+"\",\"numcuenta\":\""+"La cédula no tiene asociada una cuenta."+"\"}";
        }
    }

    @GetMapping("/all")
    public String consultarTodo() throws ClassNotFoundException, SQLException {

        List<cancion> canciones = c.all();

        return new Gson().toJson(canciones);

    }

    @PostMapping(path = "/generos", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  
    // definición
    public String actualizarCancion(@RequestBody String Cancion) throws ClassNotFoundException, SQLException {

        cancion f = new Gson().fromJson(Cancion, cancion.class); // recibimos el json y lo devolvemos un objeto cancion
        c.setId_cancion(f.getId_cancion()); // obtengo el id de f y se lo ingreso a c
        c.setNombre_cancion(f.getNombre_cancion());
        c.setAnio(f.getAnio());
        c.setFormato(f.getFormato());
        c.setRuta(f.getRuta());
        c.setAutor(f.getAutor());
        c.setGenero(f.getGenero());
        c.actualizar();

        return new Gson().toJson(c); // cliente le envia json al servidor. fpr de comunicarse

    }

    @PutMapping(path = "/nueva_cancion", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  
    // definición
    public String nuevaCancion(@RequestBody String Cancion) throws ClassNotFoundException, SQLException {

        cancion f = new Gson().fromJson(Cancion, cancion.class); // recibimos el json y lo devolvemos un objeto cancion
        c.setNombre_cancion(f.getNombre_cancion());
        c.setAnio(f.getAnio());
        c.setFormato(f.getFormato());
        c.setRuta(f.getRuta());
        c.setAutor(f.getAutor());
        c.setGenero(f.getGenero());
        c.guardar();

        return new Gson().toJson(c); // cliente le envia json al servidor. fpr de comunicarse
    }

    @DeleteMapping("/eliminar_cancion/{id}")
    public String borrarCuenta(@PathVariable("id") int id_cancion) throws ClassNotFoundException, SQLException {
        c.setId_cancion(id_cancion);
        c.borrar();
        
        return "Registro eliminado";
    }       
}
